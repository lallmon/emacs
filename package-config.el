(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl (warn "Your version of Emacs does not support
SSL connections, which is unsafe because it allows
man-in-the-middle attacks.  There are two things you can do about
this warning: 1. Install an Emacs version that does support SSL
and be safe.  2. Remove this warning from your init file so you
won't see it again."))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t))
(package-initialize)
;;Check for use-package and if not, install it
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(require 'use-package-ensure)
(setq use-package-always-ensure t)
;; styling
(use-package solarized-theme)
(use-package rainbow-delimiters)
;; general development
(use-package magit)
(use-package flycheck)
;; web development
(use-package web-mode)
(use-package emmet-mode)
(use-package prettier-js)
(use-package add-node-modules-path)
;; arduino
(use-package arduino-mode)
;; clojure
(use-package cider)
;; other
(use-package ivy)
(use-package helm
  :config
  (helm-mode 1))
(use-package projectile
  :config
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (setq projectile-project-search-path '("~/Projects/"))
  (setq projectile-ignored-projects '("~/" "/tmp/" "~/Dropbox/"))
  (setq projectile-sort-order 'recently-active)
  (setq projectile-completion-system 'ivy)
  (projectile-mode +1))
